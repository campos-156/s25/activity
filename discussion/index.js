const http = require('http');

const host = 4000;


//create a mock database
const products = [
{
    "name":"item1",
    "price": "100.00"
},
{
    "name":"item2",
    "price": "200.00"
},
{
    "name":"item3",
    "price": "300.00"
},
{
    "name":"item4",
    "price": "400.00"
},
{
    "name":"item5",
    "price": "1000.00"
},    
];

const users = [];

let server = http.createServer((req,res) => {
    if (req.url === '/') {
        res.writeHead(200, {'Content-Type':'text/plain'})

        
        res.end('Landing');
        
    } else if (req.url == '/catalog' && req.method === 'GET') {
        
        res.writeHead(200,{'Content-Type':'application/json'})
        res.write(JSON.stringify(products));
        res.end()
        
    } else if (req.url == '/register' && req.method === 'POST') {
        res.writeHead(201, {'Content-Type':'text/plain'})
        res.end('This is Register')
        
    } else if (req.url == '/admin' && req.method === 'GET') {

        res.writeHead(403, {'Content-Type':'text/plain'})

        res.end(JSON.stringify(users))
        
    }else {
        res.writeHead(404, {'Content-Type':'text/plain'});
        res.end('No page was Found');
        
    }
})

server.listen(host);

console.log(`Listening on port: ${host}`);